import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

// Angular Material
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit, AfterViewInit {

  listUsuarios: UsuarioDataI[] = [];
  
  
  displayedColumns: string[] = ['usuario', 'nombre', 'apellido', 'sexo', 'acciones'];
  dataSource!: MatTableDataSource<any>;
  
  // este viewchild es para la comunicaión entre componentes, es para que la paginación funcione
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @ViewChild(MatSort) sort!: MatSort;

  // inyectamos el UsuarioService que contiene los datos
  constructor(
    private _usuarioService: UsuarioService,
    private _snackbar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.cargarUsuarios();
  }

  // Este ciclo de vida se se ejecuta antes que todo se cargue para poder trabajar
  ngAfterViewInit(): void {
    // al dataSource como le instanciamos 'new MatTableDataSource' en cargarUsuarios(), esta variable ya tiene nuevas propiedades y por eso usamos paginator
    this.dataSource.paginator = this.paginator; // lo llenamos al dataScource con lo que tiene el paginator

    // nos va ordenar alfabéticamente
    this.dataSource.sort = this.sort;
  }

  cargarUsuarios() {
    // obtenemos a los usuarios y no usamos un subscribe porque no es un observable y el Observable trabaja con subscribe, y solamente estamos pasando retornando un método(getusuario) que te devuelve una copia
    this.listUsuarios = this._usuarioService.getUsuario();
    this.dataSource = new MatTableDataSource(this.listUsuarios);

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // pasamos como parámetro de tipo event
  applyFilter(event: Event) {
    // con 'as' casteamos, convertirmos a un tipo elemento html, event.target, selecciona al objeto del evento
    const filterValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.trim().toLowerCase();

    console.log(event.target);
    console.log(filterValue);
    
  }

  eliminarUsuario(usuario: string) { // index: number
    // console.log(index);
    
    // creamos un alert de confirmación
    const opcion = confirm(`¿Esta seguro de eliminar el usuario:  ${usuario}?`);

    if (opcion) {
      console.log(usuario);
      // llamamos al eliminar usuario que se encuentra en el service
      this._usuarioService.eliminarUsuario(usuario);
      // cargamos los usuarios actualizados
      this.cargarUsuarios();
      
      this._snackbar.open("El usuario fue eliminado con exito","Aceptar", {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      })
    }
  }

  verUsuario(usuario: string): void {
    console.log(usuario);
    this.router.navigate(['dashboard/ver-usuario', usuario])
  }

  modificarUsuario(usuario: string) {
    console.log(usuario);
    this.router.navigate(['/dashboard/crear-usuario', usuario]);
  }

}
