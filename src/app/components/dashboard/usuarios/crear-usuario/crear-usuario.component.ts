import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.scss']
})
export class CrearUsuarioComponent implements OnInit {

  tiles: any[] = [
    {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
    {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'},
  ];
    
  sexo: any[] = ['Masculino', 'Femenino'];
  adicionar:boolean = true;
  titulo = 'Crear Usuario';

  form!: FormGroup;

  // Router lo usamos para retornar a una ruta en específica
  constructor(
      private fb: FormBuilder,
      private _usuarioService: UsuarioService,
      private router: Router,
      private _snackbar: MatSnackBar,
      private activateRouted: ActivatedRoute
  ) { 
    // this.insertarUsuario();
    
    // Estos códigos son también para editar usuario
    this.activateRouted.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
      
      this.form = this.fb.group({
        usuario: ['', Validators.required],
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        sexo: ['', Validators.required]
      })
      
      // si el id de la ruta es distinto de 'nuevo'
      if (id != 'nuevo') {
        // entramos en el buscar usuario para obtener el usuario que queremos editar
        const usuario = this._usuarioService.buscarUsuario(id);
        console.log(usuario);
            
        // Verifico que la longituf del objeto es cero, esto es si alguien inyecta algun usuario que no existe u otra cosa en la url pues le mandamos al dashboard de usuarios
        if (Object.keys(usuario).length === 0) {
          this.router.navigate(['/dashboard/usuarios']);
        }

        this.form.patchValue({
          usuario: usuario.usuario,
          nombre: usuario.nombre,
          apellido: usuario.apellido,
          sexo: usuario.sexo
        });

        // estas variables es para verificar si estoy entrando a modificar un usuario y ahi cambiamos los títulos de los formularios de crear y modificar
        this.adicionar = false;
        this.titulo = 'Modificar Usuario';

      }
    
    })

  
  }

  ngOnInit(): void {
  }


  /* insertarUsuario() {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      sexo: ['', Validators.required]
    })
  } */

  agregarUsuario(): void {

    // si no es valido el formulario, entonces con return le decimos que no haga nada
    if (!this.form.valid) {
      return;
    }

    console.log(this.form.value);
    
    const user: UsuarioDataI = {
      usuario: this.form.value.usuario,
      nombre: this.form.value.nombre,
      apellido: this.form.value.apellido,
      sexo: this.form.value.sexo
    }

    console.log(user);

    // Este código también es para editar un usuario
    if (this.adicionar) {
      // al servicio agregamos el nuevo usuario
      this._usuarioService.agregarUsuario(user);
      
      // redireccionamos a la tabla de usuarios una vez agregados el nuevo user
      this.router.navigate(['/dashboard/usuarios']);

      // mensaje de exito de agregar usuario
      this._snackbar.open('El usuario fue agregado con exito', 'Aceptar', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      })
      
    } else {
      this._usuarioService.modificarUsuario(user);
      this.router.navigate(['/dashboard/usuarios']);
      
      this._snackbar.open('El usuario fue modificado con exito', 'Aceptar', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });

    }
  }


  Volver(): void {
    this.router.navigate(['/dashboard/usuarios'])
  }

}
