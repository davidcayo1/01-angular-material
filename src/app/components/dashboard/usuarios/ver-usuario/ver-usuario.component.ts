import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-ver-usuario',
  templateUrl: './ver-usuario.component.html',
  styleUrls: ['./ver-usuario.component.scss']
})
export class VerUsuarioComponent implements OnInit {

  form!: FormGroup;

  constructor(
    private activateRoute: ActivatedRoute,
    private usuarioService: UsuarioService,
    private router: Router,
    private fb: FormBuilder
  ) {

    this.activateRoute.params.subscribe(params => {
      const id = params['id']; // capturamos el nombre de usuario como id. Este ['id'] es el id de la ruta de dashboard-routing.module -> 'ver-usuario/:id', si  al id le pongo apellido por ejemplo debo pornerlo aqui también
      console.log(id);

      const usuario = this.usuarioService.buscarUsuario(id);
      console.log(usuario);
      
      // Verifico que la longitud del objeto es cero. key es para manejar json, sin no le ponemos el key no podemos hacer el .length
      if (Object.keys(usuario).length === 0) {
        // volvemos a la ruta usuarios si no hay datos en el json
        this.router.navigate(['/dashboard/usuarios']);
      }

      // si el json no es cero, entonces recien hacemos operaciones
      this.form = this.fb.group({
        usuario: ['', Validators.required],
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        sexo: ['', Validators.required]
      });

      // pathValue lo usamos para modificar los datos del usuario
      this.form.patchValue({
        usuario: usuario.usuario,
        nombre: usuario.nombre,
        apellido: usuario.apellido,
        sexo: usuario.sexo
      })

    })
  }

  ngOnInit(): void {
  }

  Volver(): void {
    this.router.navigate(['/dashboard/usuarios'])
  }
}
