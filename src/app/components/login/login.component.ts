import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// importamos los que se necesita de ReactiveForms
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Interfaces
import { UsuarioI } from 'src/app/interfaces/usuario.interface';

// Angular Material
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  loading = false;
  
  // declaramos en una variable el FormBuilder para formularios reactivos
  // declaramos un snackbar de angular material
  constructor(
      private fb: FormBuilder,
      private _snackbar: MatSnackBar,
      private router: Router)
  {
    // Llamamos a los métodos
    this.formulario();
  }

  ngOnInit(): void {
  }

  // Validamos los campor del formulario
  formulario(): void {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  // Método para validar incio de sesión
  Ingresar(): void {
    console.log(this.form);
    
    // usamos la interfaz de usuario
    const Usuario: UsuarioI = {
      usuario: this.form.value.usuario,
      password: this.form.value.password
    }

    console.log(Usuario);
    
    if (Usuario.usuario === 'jperez' && Usuario.password === 'admin123') {
      // Redireccionamos al dashboard
      this.fakeLoading(); // usamos el loading para cargar
      
    } else {
      // Mostramos un mensaje de error
      this.error();
      this.form.reset(); // va limpiar el formulario
    }
  }

  // Mensaje de error al logearse
  error(): void {
    // Estas opciones son para mostrar el mensaje al logearse
    this._snackbar.open('Usuario o contraseña incorecto', 'Error', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    })
  }

  // Método que simula en loading de iniciar sesión
  fakeLoading(): void {
    this.loading = true; // se vuelve true para mostrar el spinner
    
    setTimeout(() => {
      this.loading = false;
      // Redireccionamos al dashboard
      this.router.navigate(['dashboard']);
    }, 1500)
  }

}
